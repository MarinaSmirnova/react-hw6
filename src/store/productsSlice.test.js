import productsReducer, { setProducts } from "../store/productsSlice";

test("products reducer setter", () => {
  const initialState = {
    data: [],
  };

  const productsTestingArray = [
    {
      name: "Nike Court Vision Low Next Nature",
      price: 87,
      url: "https://static.nike.com/a/images/t_PDP_1728_v1/f_auto,q_auto:eco/a00316f2-6ab5-480a-a244-f6af9c253ed6/buty-court-vision-low-next-nature-mM8fv4.png",
      article: 315401,
      key: 315401,
      color: "white",
    },
    {
      name: "Nike Air Force 1",
      price: 87,
      url: "https://static.nike.com/a/images/t_PDP_1728_v1/f_auto,q_auto:eco/6465ceb1-a9a9-48b5-874f-438c25b0563d/buty-air-force-1-pltaform-Gms5Jl.png",
      article: 315305,
      key: 315305,
      color: "white",
    },
  ];

  const action = setProducts(productsTestingArray);

  const testState = productsReducer(initialState, action);

  expect(testState).toEqual({ data: productsTestingArray });
});
