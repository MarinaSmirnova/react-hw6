import modalReducer, { setModalType } from "../store/modalSlice";

test("modal reducer setter", () => {
  const initialState = {
    modalType: "None",
  };

  const action = setModalType("TEST");

  const testState = modalReducer(initialState, action);

  expect(testState).toEqual({ modalType: "TEST" });
});
