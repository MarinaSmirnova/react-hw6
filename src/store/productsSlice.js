import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const fetchDataOfProducts = createAsyncThunk(
  "products/fetchDataOfProducts",
  async (_, { dispatch }) => {
    try {
      const response = await fetch("http://localhost:3000/products.json");
      const productsData = await response.json();

      const productsWithKeys = productsData.map((product) => ({
        ...product,
        key: product.article.toString(),
      }));

      dispatch(setProducts(productsWithKeys));

      return productsWithKeys;
    } catch (e) {
      console.log("Помилка при виконанні fetch-запиту: ", e);
    }
  }
);

const productsSlice = createSlice({
  name: "products",
  initialState: {
    data: [],
  },
  reducers: {
    setProducts: (state, action) => {
      state.data = action.payload;
    },
  },
});

export const { setProducts } = productsSlice.actions;

export default productsSlice.reducer;
