import * as yup from 'yup';

export const orderSchema = yup.object({
    firstName: yup.string().min(2, "First Name is too short").required("Please, enter your First Name"),
    lastName: yup.string().min(2, "Last Name is too short").required("Please, enter your Last Name"),
    age: yup.number().min(14, "You are too young to place an order").max(105, "Enter your real age").required("Please, enter your age"),
    telNumber: yup.string().matches(/^\+380 \(\d{2}\) \d{3}-\d{2}-\d{2}$/, "Enter a correct phone number").required("Please, enter your phone number"),
    adress: yup.string().min(10, "Enter your full adress").required("Please, enter your adress"),
});