import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { setModalType } from "../store/modalSlice";
import modalSlice from "../store/modalSlice";

function Modal({ header, closeButton = true, text, actions }) {
  const dispatch = useDispatch();

  const closeModal = () => {
    dispatch(setModalType("None"));
  };

  useEffect(() => {
    document.body.classList.add("modal-is-open");
    return () => {
      document.body.classList.remove("modal-is-open");
    };
  }, []);

  return (
    <div
      className="modal__overlay"
      onClick={(e) => {
        if (e.target === e.currentTarget) {
          closeModal();
        }
      }}
    >
      <div className="modal__div">
        <div className="modal__header">
          <h1 className="modal__header__text">{header}</h1>
          {closeButton === true && (
            <button
              className="modal__header__close-button"
              onClick={closeModal}
            >
              X
            </button>
          )}
        </div>
        <p className="modal__text">{text}</p>
        <div className="modal__button-container">{actions}</div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  actions: PropTypes.node,
  closeButton: PropTypes.bool,
};

Modal.defaultProps = {
  closeButton: true,
  actions: null,
};

export default Modal;
