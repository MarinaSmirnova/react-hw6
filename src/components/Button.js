import React from "react";
import PropTypes from "prop-types";

function Button({ backgroundColor, text, type = "button", clickFunc }) {
  const styleProps = {
    backgroundColor: backgroundColor,
  };

  return (
    <button
      className="button"
      type={type}
      onClick={clickFunc}
      style={styleProps}
    >
      {text}
    </button>
  );
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string,
};

Button.defaultProps = {
  backgroundColor: undefined,
};

export default Button;
