import { Formik, Form, Field } from "formik";
import { orderSchema } from "../schemes/orderSchema";
import Button from "./Button";
import { PatternFormat } from "react-number-format";

function OrderForm({ onSubmitFunc }) {
  const initialValues = {
    firstName: "",
    lastName: "",
    age: "",
    telNumber: "",
    adress: "",
  };

  const handleSubmit = (values, actions) => {
    const localCartArray = JSON.parse(localStorage.getItem("productsInCart"));

    if (localCartArray) {
      const products = localCartArray;
      console.log("ORDER INFO: ", values, products);

      onSubmitFunc();
    } else {
      console.log("Cart is empty");
    }

    actions.setSubmitting(false);
    actions.resetForm();
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={orderSchema}
      onSubmit={handleSubmit}
    >
      {(formikProps) => (
        <Form className="order-form">
          <div className="order-form__name">
            <div className="order-form__first-name order-form__block">
              <label htmlFor="firstName">First Name</label>
              <Field
                type="text"
                name="firstName"
                maxLength={15}
                onChange={(e) => {
                  formikProps.setFieldValue(
                    "firstName",
                    e.target.value.replace(/[^a-zA-Zа-яА-Я\s-]/g, "")
                  );
                }}
              />
              {formikProps.touched.firstName &&
                formikProps.errors.firstName && (
                  <p className="order-form__error-msg">
                    {formikProps.errors.firstName}
                  </p>
                )}
            </div>
            <div className="order-form__last-name order-form__block">
              <label htmlFor="lastName">Last Name</label>
              <Field
                type="text"
                name="lastName"
                maxLength={15}
                onChange={(e) => {
                  formikProps.setFieldValue(
                    "lastName",
                    e.target.value.replace(/[^a-zA-Zа-яА-Я\s-]/g, "")
                  );
                }}
              />
              {formikProps.touched.lastName && formikProps.errors.lastName && (
                <p className="order-form__error-msg">
                  {formikProps.errors.lastName}
                </p>
              )}
            </div>
          </div>
          <div className="order-form__age order-form__block">
            <label htmlFor="age">Age</label>
            <Field
              type="number"
              name="age"
              onChange={(e) => {
                formikProps.setFieldValue(
                  "age",
                  e.target.value.replace(/[^\d.,e]/g, "").slice(0, 3)
                );
              }}
            />
            {formikProps.touched.age && formikProps.errors.age && (
              <p className="order-form__error-msg">{formikProps.errors.age}</p>
            )}
          </div>
          <div className="order-form__tel-number order-form__block">
            <label htmlFor="telNumber">Phone number</label>
            <Field name="telNumber">
              {({ field }) => (
                <PatternFormat
                  {...field}
                  type="tel"
                  format="+380 (##) ###-##-##"
                  mask="_"
                  allowEmptyFormatting
                />
              )}
            </Field>
            {formikProps.touched.telNumber && formikProps.errors.telNumber && (
              <p className="order-form__error-msg">
                {formikProps.errors.telNumber}
              </p>
            )}
          </div>
          <div className="order-form__adress order-form__block">
            <label htmlFor="adress">Adress</label>
            <Field as="textarea" name="adress" maxLength={90} />
            {formikProps.touched.adress && formikProps.errors.adress && (
              <p className="order-form__error-msg">
                {formikProps.errors.adress}
              </p>
            )}
          </div>
          <Button type="submit" text="Checkout" />
        </Form>
      )}
    </Formik>
  );
}

export default OrderForm;
