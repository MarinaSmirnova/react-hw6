import React, { useState } from "react";
import useProductsDisplay from "../context/productsDisplay";

function DisplayToggle() {
  const { productsDisplayMode, cardsDisplay, tableDisplay } =
    useProductsDisplay();
  const [isChecked, setIsChecked] = useState(productsDisplayMode === "table");

  const onChangeToggle = (e) => {
    const cardsDisplayStatus = e.currentTarget.checked;
    if (cardsDisplayStatus) {
      setIsChecked(true);
      tableDisplay();
    } else {
      setIsChecked(false);
      cardsDisplay();
    }
  };

  return (
    <div className="toggle__wrapper">
      <span className="toggle__text">{"Cards"}</span>
      <input
        type="checkbox"
        className="toggle__input"
        id="toggle"
        value=""
        onChange={onChangeToggle}
        checked={isChecked}
      />
      <label htmlFor="toggle" className="toggle__container">
        <div className="toggle__slider"></div>
      </label>
      <span className="toggle__text">{"Table"}</span>
    </div>
  );
}

export default DisplayToggle;
