import React from "react";
import PropTypes from "prop-types";

function Header({ elements }) {
  return (
    <header className="header">
      <div className="header__logo">- Brand sports shoes -</div>
      <div className="header__icons">{elements}</div>
    </header>
  );
}

Header.propTypes = {
  elements: PropTypes.element,
};

Header.defaultProps = {
  elements: null,
};

export default Header;
