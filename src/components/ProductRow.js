import React from "react";
import PropTypes from "prop-types";

function ProductRow({ rowData, buttons }) {
  const { name, url, color, price, article } = rowData;

  return (
    <tr>
      <td className="img-td">
        <img className="product__img" alt={name} src={url} />
      </td>
      <td className="name-td">
        <h3 className="product__name">{name}</h3>
      </td>
      <td className="color-td">
        <p className="product__color">{color}</p>
      </td>
      <td className="article-td">
        <p className="product__article">{article}</p>
      </td>
      <td className="price-td">
        <h3 className="product__price">€ {price}</h3>
      </td>
      <td className="buttons-td">
        <div className="product__buttons">{buttons}</div>
      </td>
    </tr>
  );
}

ProductRow.propTypes = {
  rowData: PropTypes.shape({
    key: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    article: PropTypes.number.isRequired,
  }).isRequired,
  buttons: PropTypes.element,
};

ProductRow.defaultProps = {
  buttons: null,
};

export default ProductRow;
