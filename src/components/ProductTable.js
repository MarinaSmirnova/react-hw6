import React from "react";
import PropTypes from "prop-types";

function ProductTable({ productRows }) {
  return (
    (
      <table className="products-table">
        <thead>
          <tr>
            <th>Photo</th>
            <th>Product Name</th>
            <th>Color</th>
            <th>Article</th>
            <th>Price</th>
            <th>Buy This</th>
          </tr>
        </thead>
        <tbody>{productRows}</tbody>
      </table>
    ) || "No products"
  );
}

ProductTable.propTypes = {
  productRows: PropTypes.node,
};

export default ProductTable;
