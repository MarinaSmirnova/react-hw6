import Button from "../Button";
import { render, screen, fireEvent } from "@testing-library/react";

describe("Button component", () => {
    test("Button test, clickFunc function", () => {
        const clickFuncEmulation = jest.fn();
        const text = "Testing Button";
    
        render(<Button clickFunc={clickFuncEmulation} text={text} />);
        const button = screen.getByText(text);
        fireEvent.click(button);
        expect(clickFuncEmulation).toHaveBeenCalledTimes(1);
    });

    test("Snapshot", () => {
        const clickFuncEmulation = jest.fn();

        const button = render(<Button clickFunc={clickFuncEmulation} text="Test"/>);
        expect(button).toMatchSnapshot();
    });
});
