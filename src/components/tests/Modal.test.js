import "@testing-library/jest-dom/extend-expect";
import Modal from "../Modal";
import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../../store";

describe("Modal component", () => {
  test("Render modal component content with close button ", () => {
    render(
      <Provider store={store}>
        <Modal
          header={"Testing Modal"}
          closeButton={true}
          text={"Text for testing modal"}
          actions={null}
        />
      </Provider>
    );

    const modalHeader = screen.getByText("Testing Modal");
    expect(modalHeader).toBeInTheDocument();

    const modalText = screen.getByText("Text for testing modal");
    expect(modalText).toBeInTheDocument();

    const closeBtn = screen.getByText("X");
    expect(closeBtn).toBeInTheDocument();
  });

  test("Don't render close button when 'closeButton' is false", () => {
    render(
      <Provider store={store}>
        <Modal
          header={"Testing Modal"}
          closeButton={false}
          text={"Text for testing modal"}
          actions={null}
        />
      </Provider>
    );

    const closeBtn = screen.queryByText("X");
    expect(closeBtn).toBeNull();
  });

  test("Snapshot", () => {

    const modal = render(
      <Provider store={store}>
        <Modal
          header={"Testing Modal"}
          closeButton={true}
          text={"Text for testing modal"}
          actions={null}
        />
      </Provider>
    );
    expect(modal).toMatchSnapshot();
  });
});
