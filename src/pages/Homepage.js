import React, { useState, useEffect } from "react";
import ProductList from "../components/ProductList";
import ProductCard from "../components/ProductCard";
import Button from "../components/Button";
import Modal from "../components/Modal";
import Star from "../components/Star";
import { fetchDataOfProducts } from "../store/productsSlice";
import { useDispatch, useSelector } from "react-redux";
import { setModalType } from "../store/modalSlice";
import DisplayToggle from "../components/DisplayToggle";
import { ProductsDisplayProvider } from "../context/productsDisplay";
import ProductTable from "../components/ProductTable";
import ProductRow from "../components/ProductRow";

const Homepage = () => {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products.data);
  const modalType = useSelector((state) => state.modal.modalType);

  const displayModeInSessionStorage =  sessionStorage.getItem("displayMode");

  const [activeProduct, setActiveProduct] = useState("");
  const [productsDisplayMode, setProductsDisplayMode] = useState(displayModeInSessionStorage || "cards");

  const cardsDisplay = () => {
    setProductsDisplayMode("cards");
    sessionStorage.setItem("displayMode", "cards");
  };

  const tableDisplay = () => {
    setProductsDisplayMode("table");
    sessionStorage.setItem("displayMode", "table");
  };

  useEffect(() => {}, [productsDisplayMode]);

  const checkLocalArray = (array) => {
    if (!localStorage.getItem(array)) {
      localStorage.setItem(array, JSON.stringify([]));
    }
  };

  checkLocalArray("productsInCart");
  checkLocalArray("likedProducts");

  const checkActiveProduct = (product) => {
    setActiveProduct(product);
  };

  const checkStar = (product) => {
    if (checkProductInArray(product, "likedProducts")) {
      const likedProductsArray = JSON.parse(
        localStorage.getItem("likedProducts")
      );

      const newLikedProductsArray = likedProductsArray.filter(
        (productFromArray) => productFromArray.key !== product.key
      );

      localStorage.setItem(
        "likedProducts",
        JSON.stringify(newLikedProductsArray)
      );
    } else {
      addProductToArray(product, "likedProducts");
    }
  };

  const checkProductInArray = (product, array) => {
    const localArray = JSON.parse(localStorage.getItem(array));
    const findProductInArray = localArray.find((productInLokalStarage) =>
      productInLokalStarage.key.includes(product.key)
    );

    return findProductInArray ? true : false;
  };

  const addProductToArray = (product, array) => {
    const productArray = JSON.parse(localStorage.getItem(array));

    productArray.push(product);
    const arrayInJSON = JSON.stringify(productArray);

    localStorage.setItem(array, arrayInJSON);

    dispatch(setModalType("None"));
    setActiveProduct("");
  };

  const openConfirmModal = (product) => {
    dispatch(setModalType("Confirm"));
    checkActiveProduct(product);
  };

  const addProductToCart = (product) => {
    const localCartArray = JSON.parse(localStorage.getItem("productsInCart"));

    if (checkProductInArray(product, "productsInCart")) {
      const index = localCartArray.findIndex(function (productInCartArray) {
        return productInCartArray.key === product.key;
      });

      localCartArray[index].quantity++;
    } else {
      const productClone = JSON.parse(JSON.stringify(product));
      productClone.quantity = 1;

      localCartArray.push(productClone);
    }

    localStorage.setItem("productsInCart", JSON.stringify(localCartArray));

    dispatch(setModalType("None"));
    setActiveProduct("");
  };

  useEffect(() => {
    dispatch(fetchDataOfProducts());
  }, [checkStar]);

  return (
    <>
      <ProductsDisplayProvider
        value={{ productsDisplayMode, cardsDisplay, tableDisplay }}
      >
        <DisplayToggle />
        {productsDisplayMode === "cards" && (
          <ProductList
            productCards={
              <>
                {products.map((product) => (
                  <ProductCard
                    cardData={product}
                    buttons={
                      <>
                        <Star
                          clickFunc={() => checkStar(product)}
                          type={
                            checkProductInArray(product, "likedProducts")
                              ? "Painted"
                              : "Empty"
                          }
                        />
                        <Button
                          clickFunc={() => openConfirmModal(product)}
                          text="Add to cart"
                        />
                      </>
                    }
                  />
                ))}
              </>
            }
          />
        )}
        {productsDisplayMode === "table" && (
          <ProductTable
            productRows={
              <>
                {products.map((product) => (
                  <ProductRow
                    rowData={product}
                    buttons={
                      <>
                        <Star
                          clickFunc={() => checkStar(product)}
                          type={
                            checkProductInArray(product, "likedProducts")
                              ? "Painted"
                              : "Empty"
                          }
                        />
                        <Button
                          clickFunc={() => openConfirmModal(product)}
                          text="Add to cart"
                        />
                      </>
                    }
                  />
                ))}
              </>
            }
          />
        )}
      </ProductsDisplayProvider>
      {modalType === "Confirm" && (
        <Modal
          header="Add to cart"
          closeButton={true}
          text="Would you like to add this item to your cart?"
          actions={
            <>
              <Button
                clickFunc={() => addProductToCart(activeProduct)}
                text="Yes"
              />
              <Button
                clickFunc={() => dispatch(setModalType("None"))}
                text="No"
              />
            </>
          }
        />
      )}
    </>
  );
};

export { Homepage };
