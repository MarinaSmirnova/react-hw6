import { createContext, useContext } from "react";

export const ProductsDisplayContext = createContext({
  productsDisplayMode: "cards",
});

export const ProductsDisplayProvider = ProductsDisplayContext.Provider;

export default function useProductsDisplay() {
  return useContext(ProductsDisplayContext);
}
