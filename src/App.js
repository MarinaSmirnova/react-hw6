import React, { useState, useEffect } from "react";
import {Routes, Route} from "react-router-dom";
import "./App.scss";

import { Layout } from "./components/Layout";

import { Homepage } from "./pages/Homepage";
import { Cart } from "./pages/Cart";
import { Likespage } from "./pages/Likespage";
import { Notfoundpage } from "./pages/Notfoundpage";


function App() {

  const checkLocalArray = (array) => {
    if (!localStorage.getItem(array)) {
      localStorage.setItem(array, JSON.stringify([]));
    }
  };

  checkLocalArray("productsInCart");
  checkLocalArray("likedProducts");

  return (
    <>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route path="/" element={<Homepage />} />
          <Route path="cart" element={<Cart />} />
          <Route path="likes" element={<Likespage />} />
          <Route path="*" element={<Notfoundpage />} />
        </Route>
      </Routes>
    </>
  );
}

export default App;
